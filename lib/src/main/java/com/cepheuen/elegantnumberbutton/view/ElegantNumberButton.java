package com.cepheuen.elegantnumberbutton.view;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.ResourceManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Created by Ashik Vetrivelu on 10/08/16.
 */
public class ElegantNumberButton extends DependentLayout {
    private Context context;
    private AttrSet attrs;
    private String styleAttr;
    private OnClickListener mListener;
    private int initialNumber;
    private int lastNumber;
    private int currentNumber;
    private int finalNumber;
    private Text textView;
    private OnValueChangeListener mOnValueChangeListener;

    public Button addBtn, subtractBtn;

    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, ElegantNumberButton.class.getName());

    public ElegantNumberButton(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public ElegantNumberButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        this.context = context;
        this.attrs = attrSet;
        initView();
    }

    public ElegantNumberButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        this.context = context;
        this.attrs = attrSet;
        this.styleAttr = styleName;
        initView();
    }

    private void initView() {
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout, this, true);
        final ResourceManager res = getResourceManager();
        int textSize = 0;
        Color color = null;
        Color textColor = null;
        Element element = null;
        ShapeElement defaultDrawable = null;
        try {
            final int defaultColor = res.getElement(ResourceTable.Color_colorPrimary).getColor();
            int defaultTextColor = res.getElement(ResourceTable.Color_colorText).getColor();
            defaultDrawable = new ShapeElement(context, ResourceTable.Graphic_background);
            //Get custom properties
            initialNumber = attrs.getAttr("initialNumber").map(Attr::getIntegerValue).orElse(0);
            finalNumber = attrs.getAttr("finalNumber").map(Attr::getIntegerValue).orElse(Integer.MAX_VALUE);
            textSize = attrs.getAttr("textSize").map(Attr::getDimensionValue).orElse(13);
            color = attrs.getAttr("backGroundColor").map(Attr::getColorValue).orElse(new Color(defaultColor));
            textColor = attrs.getAttr("textColor").map(Attr::getColorValue).orElse(new Color(defaultTextColor));
            element = attrs.getAttr("backgroundDrawable").map(Attr::getElement).orElse(defaultDrawable);
        } catch (Exception e) {
            HiLog.error(LABEL, "Please check the color resource file in");
        }


        subtractBtn = (Button) findComponentById(ResourceTable.Id_subtract_btn);
        addBtn = (Button) findComponentById(ResourceTable.Id_add_btn);
        textView = (Text) findComponentById(ResourceTable.Id_number_counter);
        DirectionalLayout mLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_layout);

        subtractBtn.setTextColor(textColor);
        addBtn.setTextColor(textColor);
        textView.setTextColor(textColor);

        subtractBtn.setTextSize(textSize);
        addBtn.setTextSize(textSize);
        textView.setTextSize(textSize);

        assert element != null;
        ShapeElement shapeElement =new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color.getValue()));
        mLayout.setBackground(shapeElement);

        textView.setText(String.valueOf(initialNumber));

        currentNumber = initialNumber;
        lastNumber = initialNumber;

        subtractBtn.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                int num = Integer.valueOf(textView.getText().toString());
                setNumber(String.valueOf(num - 1), true);
            }
        });
        addBtn.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                int num = Integer.valueOf(textView.getText().toString());
                setNumber(String.valueOf(num + 1), true);
            }
        });
    }


    private void callListener(Component view) {
        if (mListener != null) {
            mListener.onClick(view);
        }

        if (mOnValueChangeListener != null) {
            if (lastNumber != currentNumber) {
                mOnValueChangeListener.onValueChange(this, lastNumber, currentNumber);
            }
        }
    }

    public String getNumber() {
        return String.valueOf(currentNumber);
    }

    public void setNumber(String number) {
        lastNumber = currentNumber;
        this.currentNumber = Integer.parseInt(number);
        if (this.currentNumber > finalNumber) {
            this.currentNumber = finalNumber;
        }
        if (this.currentNumber < initialNumber) {
            this.currentNumber = initialNumber;
        }
        textView.setText(String.valueOf(currentNumber));
    }

    public void setNumber(String number, boolean notifyListener) {
        setNumber(number);
        if (notifyListener) {
            callListener(this);
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.mListener = onClickListener;
    }

    public void setOnValueChangeListener(OnValueChangeListener onValueChangeListener) {
        mOnValueChangeListener = onValueChangeListener;
    }

    public interface OnClickListener {
        void onClick(Component view);
    }

    public interface OnValueChangeListener {
        void onValueChange(ElegantNumberButton view, int oldValue, int newValue);
    }

    public void setRange(Integer startingNumber, Integer endingNumber) {
        this.initialNumber = startingNumber;
        this.finalNumber = endingNumber;
    }

    public void updateColors(Element backgroundColor, Color textColor) {
        this.textView.setBackground(backgroundColor);
        this.addBtn.setBackground(backgroundColor);
        this.subtractBtn.setBackground(backgroundColor);

        this.textView.setTextColor(textColor);
        this.addBtn.setTextColor(textColor);
        this.subtractBtn.setTextColor(textColor);
    }

    public void updateTextSize(Text.TextSizeType unit, float newSize) {
        this.textView.setTextSize((int) newSize, unit);
        this.addBtn.setTextSize((int) newSize, unit);
        this.subtractBtn.setTextSize((int) newSize, unit);
    }

}
