package com.cepheuen.elegantnumberbuttonsample;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MainAbilityTest {
    private Ability mAbility;
    private ElegantNumberButton btn1;
    private Text mText;

    @Before
    public void setUp() throws Exception {
        mAbility = EventHelper.startAbility(MainAbility.class);
        Thread.sleep(2000);
        btn1 = (ElegantNumberButton) mAbility.findComponentById(ResourceTable.Id_number_button);
        mText = (Text) mAbility.findComponentById(ResourceTable.Id_text_view);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void test_add_button_click() throws Exception {
        mAbility.getUITaskDispatcher().syncDispatch(new Runnable() {
            @Override
            public void run() {
                btn1.addBtn.setText("target");
            }
        });
        EventHelper.triggerClickEvent(mAbility, btn1.addBtn);
        Thread.sleep(1000);
        Assert.assertEquals(btn1.getNumber(), "1");
    }

    @Test
    public void test_set_number() throws Exception {
        mAbility.getUITaskDispatcher().syncDispatch(new Runnable() {
            @Override
            public void run() {
                btn1.setNumber("3", true /*notify listeners*/);
                btn1.setNumber("1", true /*don't notify*/);
            }
        });
        Thread.sleep(1000);
        Assert.assertEquals(mText.getText(), "3");
    }
}