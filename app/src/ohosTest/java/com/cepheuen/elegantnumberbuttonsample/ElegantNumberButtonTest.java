/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cepheuen.elegantnumberbuttonsample;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import junit.framework.TestCase;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ElegantNumberButtonTest extends TestCase {
    private Ability mAbility;
    private ElegantNumberButton btn1;
    private Text mText;
    private String notifyListenerIsTrue = null;
    private String notifyListenerIsFalse = null;

    @Before
    public void setUp() throws Exception {
        mAbility = EventHelper.startAbility(MainAbility.class);
        Thread.sleep(2000);
        btn1 = (ElegantNumberButton) mAbility.findComponentById(ResourceTable.Id_number_button);
        mText = (Text) mAbility.findComponentById(ResourceTable.Id_text_view);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void testOnGetNumber() {
        EventHelper.triggerClickEvent(mAbility, btn1.addBtn);
        String value = btn1.getNumber();
        Assert.assertEquals(value, "1");
    }

    @Test
    public void testOnSetNumber() throws Exception {
        mAbility.getUITaskDispatcher().syncDispatch(new Runnable() {
            @Override
            public void run() {
                btn1.setNumber("3", true);
            }
        });
        notifyListenerIsTrue = btn1.getNumber();
        Thread.sleep(4000);
        Assert.assertTrue(notifyListenerIsTrue.equals("3"));
    }

    @Test
    public void testOnSetRange() {
        mAbility.getUITaskDispatcher().syncDispatch(new Runnable() {
            @Override
            public void run() {
                btn1.setRange(1, 5);
                btn1.setNumber("6");
            }
        });
        String value = btn1.getNumber();
        Assert.assertEquals(value, "5");
    }


}
