package com.cepheuen.elegantnumberbuttonsample;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

public class MainAbility extends Ability {
    public ElegantNumberButton btn1;
    public ElegantNumberButton btn2;
    public Text textView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);//Layout_ability_main

        btn1 = (ElegantNumberButton) findComponentById(ResourceTable.Id_number_button);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
        btn1.updateColors(shapeElement, Color.BLACK);
        btn2 = (ElegantNumberButton) findComponentById(ResourceTable.Id_number_button2);
        btn2.updateColors(shapeElement, Color.BLACK);

        textView = (Text) findComponentById(ResourceTable.Id_text_view);
        btn1.setRange(1, 5);
        btn1.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(Component view) {
                String number = btn1.getNumber();
                textView.setText(number);
                btn2.setNumber(number);
            }
        });
        btn1.setOnValueChangeListener((view, oldValue, newValue) -> {
            System.out.println(String.format("oldValue: %d   newValue: %d", oldValue, newValue));
        });
        btn2.setOnClickListener(new ElegantNumberButton.OnClickListener() {
            @Override
            public void onClick(Component view) {
                String number = btn2.getNumber();
                textView.setText(number);
                btn1.setNumber(number);
            }
        });
    }
}
