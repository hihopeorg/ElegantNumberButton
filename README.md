# ElegantNumberButton

**本项目是基于开源项目ElegantNumberButton进行ohos的移植和开发的，可以通过项目标签以及github地址（https://github.com/ashik94vc/ElegantNumberButton ）追踪到原项目版本**

#### 项目介绍

- 项目名称：ElegantNumberButton。
 - 所属系列：ohos的第三方组件适配移植
 - 功能：可以增减的自定义控件。
 - 项目移植状态：完成
 - 调用差异：无
 - 项目作者和维护人：hihope
 - 联系方式：hihope@hoperun.com
 - 原项目Doc地址：https://github.com/ashik94vc/ElegantNumberButton
 - 编程语言：Java
 - 外部库依赖：无
 - 原项目基线版本：v1.0.3 , sha1:92003f91866f8d92c2017f174dc332af9acf34e4

#### 演示效果

<img src="gif/效果动画.gif"/>

- #### 安装教程

方法1.

1. 编译ElegantNumberButton的har包: ElegantNumberButton.har。

2. 启动 DevEco Studio，将编译的har包导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
   	……
   }
   ```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    Implementation 'com.cepheuen.elegantnumberbutton.ohos:ElegantNumberButton:1.0.0'
}
```

 #### 使用说明

```xml
    <com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
        ohos:id="$+id:number_button"
        ohos:height="match_content"
        ohos:width="200vp"
        ohos:below="$id:text_view"
        ohos:horizontal_center="true"
        ohos:top_margin="18vp"
        app:backGroundColor="$color:colorPrimary"
        app:textSize="15fp"/>
```



 ```java
  	@Override
     public void onStart(Intent intent) {
         ElegantNumberButton btn1 = (ElegantNumberButton) findComponentById(ResourceTable.Id_number_button);
                 ShapeElement shapeElement=new ShapeElement();
         shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
         btn1.updateColors(shapeElement, Color.BLACK);
         btn1.setRange(1,5);
         btn1.setOnClickListener(new ElegantNumberButton.OnClickListener() {
             @Override
             public void onClick(Component view) {
                 String number = btn1.getNumber();
                 textView.setText(number);
                 btn2.setNumber(number);
             }
         });
     }
 
        
 ```



### 版本迭代

 - v1.0.0

  实现功能
 1. 带加减号的自定义控件


 #### 版权和许可信息
Copyright 2016 Ashik Vetrivelu

```
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```